package structs

import (
	"fmt"
	"sync"
)

// QueueMode defines a queue mode.
type QueueMode int

const (
	// QueueModeStrict generates an error when adding to a full queue.
	QueueModeStrict QueueMode = iota
	// QueueModeCache removes last element from the queue when adding to a full queue.
	QueueModeCache
)

// Queue implementation of a limited size FIFO queue.
type Queue struct {
	index   map[string]interface{}
	queue   []string
	size    int
	maxSize int
	nextPop int
	nextAdd int
	mode    QueueMode
	mux     *sync.Mutex
}

// NewQueue creates a new Queue.
func NewQueue(maxSize int) *Queue {
	return &Queue{
		index:   make(map[string]interface{}),
		queue:   make([]string, maxSize),
		size:    0,
		maxSize: maxSize,
		nextPop: 0,
		nextAdd: 0,
		mux:     new(sync.Mutex),
	}
}

func (q *Queue) String() string {
	return fmt.Sprintf("[size:%d][maxSize:%d][nextPop:%d][nextAdd:%d]", q.size, q.maxSize, q.nextPop, q.nextAdd)
}

// SetMode changes how the queue behaves when adding/removing elements from a full or empty queue.
func (q *Queue) SetMode(mode QueueMode) {
	q.mode = mode
}

// Add inserts a new element at the end of the queue.
// If the element's id already exists then nothing happens and no error is returned. This is an alias for the Shift method.
func (q *Queue) Add(id string, elem interface{}) error {
	q.mux.Lock()
	defer q.mux.Unlock()

	return q.shift(id, elem, false)
}

// Shift inserts a new element at the end of the queue.
// If the element's id already exists then nothing happens and no error is returned.
func (q *Queue) Shift(id string, elem interface{}) error {
	q.mux.Lock()
	defer q.mux.Unlock()

	return q.shift(id, elem, false)
}

// ShiftOrReplace inserts a new element at the end of the queue.
// If the element's id already exists then the queue's element is replaced with the one provided.
func (q *Queue) ShiftOrReplace(id string, elem interface{}) error {
	q.mux.Lock()
	defer q.mux.Unlock()

	return q.shift(id, elem, true)
}

// Unshift removes the last element of the queue and returns it.
func (q *Queue) Unshift() interface{} {
	q.mux.Lock()
	defer q.mux.Unlock()

	return q.unshift()
}

// Push inserts a new element at the head of the queue.
// If the element's id already exists then nothing happens and no error is returned.
func (q *Queue) Push(id string, elem interface{}) error {
	q.mux.Lock()
	defer q.mux.Unlock()

	return q.push(id, elem, false)
}

// PushOrReplace inserts a new element at the head of the queue.
// If the element's id already exists then the queue's element is replaced with the one provided.
func (q *Queue) PushOrReplace(id string, elem interface{}) error {
	q.mux.Lock()
	defer q.mux.Unlock()

	return q.push(id, elem, true)
}

// IsQueued checks if the given task ID is in the queue.
func (q *Queue) IsQueued(id string) bool {
	q.mux.Lock()
	defer q.mux.Unlock()

	_, found := q.index[id]
	return found
}

// Pop removes the next element from the queue and returns it.
func (q *Queue) Pop() interface{} {
	q.mux.Lock()
	defer q.mux.Unlock()

	return q.pop()
}

// Get returns the element in the position provided considering the first element (0) to be the start of the queue, where elements are Add(ed) / Shift(ed).
// This function is concurrency safe but the data returned is an interface, therefor it is up to the user to store concurrency-safe data.
func (q *Queue) Get(pos int) (string, interface{}) {
	q.mux.Lock()
	defer q.mux.Unlock()

	if pos > q.maxSize-1 {
		return "", nil
	}

	realPos := q.nextPop + pos
	if realPos >= q.maxSize {
		realPos = realPos - q.maxSize
	}

	id := q.queue[realPos]
	if id == "" {
		return id, nil
	}

	return id, q.index[id]
}

// Size returns the queue size.
func (q *Queue) Size() int {
	q.mux.Lock()
	defer q.mux.Unlock()

	return q.size
}

// Flush cleans the entire queue and resets it back to the initial state.
func (q *Queue) Flush() {
	q.mux.Lock()
	defer q.mux.Unlock()

	q.index = make(map[string]interface{})
	q.queue = make([]string, q.maxSize)
	q.nextAdd = 0
	q.nextPop = 0
	q.size = 0
}

func (q *Queue) shift(id string, elem interface{}, replace bool) error {
	if _, found := q.index[id]; found {
		if replace {
			q.index[id] = elem
		}
		return nil
	}

	if q.size >= q.maxSize {
		if q.mode == QueueModeCache {
			q.pop()
		} else {
			return fmt.Errorf("queue is full")
		}
	}

	q.queue[q.nextAdd] = id
	q.index[id] = elem

	q.nextAdd++
	q.size++
	if q.nextAdd >= q.maxSize {
		q.nextAdd = 0
	}

	return nil
}

func (q *Queue) unshift() interface{} {
	if q.size == 0 {
		return nil
	}

	q.nextAdd--
	q.size--
	if q.nextAdd < 0 {
		q.nextAdd = q.maxSize - 1
	}

	id := q.queue[q.nextAdd]
	elem := q.index[id]
	delete(q.index, id)

	return elem
}

func (q *Queue) pop() interface{} {
	if q.size == 0 {
		return nil
	}

	id := q.queue[q.nextPop]
	elem := q.index[id]
	delete(q.index, id)
	q.nextPop++
	q.size--
	if q.nextPop >= q.maxSize {
		q.nextPop = 0
	}

	return elem
}

func (q *Queue) push(id string, elem interface{}, replace bool) error {
	if _, found := q.index[id]; found {
		if replace {
			q.index[id] = elem
		}
		return nil
	}

	if q.size >= q.maxSize {
		if q.mode == QueueModeCache {
			q.unshift()
		} else {
			return fmt.Errorf("queue is full")
		}
	}

	q.nextPop--
	q.size++
	if q.nextPop < 0 {
		q.nextPop = q.maxSize - 1
	}

	q.queue[q.nextPop] = id
	q.index[id] = elem

	return nil
}
