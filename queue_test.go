package structs

import (
	"fmt"
	"testing"

	. "github.com/onsi/gomega" // for testing
)

func TestQueueGet(t *testing.T) {
	RegisterTestingT(t)

	q := NewQueue(10)
	q.SetMode(QueueModeCache)
	Expect(q.size).To(Equal(0))
	Expect(len(q.index)).To(Equal(0))

	for i := 0; i < 5; i++ {
		q.Shift(fmt.Sprintf("%d", i), i)
	}

	for i := 0; i < q.Size(); i++ {
		id, data := q.Get(i)
		Expect(id).To(Equal(fmt.Sprintf("%d", i)))
		Expect(data).To(Equal(i))
	}

	id, data := q.Get(100)
	Expect(id).To(Equal(""))
	Expect(data).To(BeNil())

	for i := 0; i < 15; i++ {
		q.Shift(fmt.Sprintf("%d", i), i)
		t.Log(q.String())
	}

	for i := 0; i < q.Size(); i++ {
		id, data := q.Get(i)
		Expect(id).To(Equal(fmt.Sprintf("%d", i+5)))
		Expect(data).To(Equal(i + 5))
	}
}

func TestUnshift(t *testing.T) {
	var err error
	var res interface{}

	RegisterTestingT(t)

	q := NewQueue(5)
	Expect(q.size).To(Equal(0))
	Expect(len(q.index)).To(Equal(0))

	res = q.Unshift()
	Expect(res).To(BeNil())

	for i := 0; i < 5; i++ {
		err = q.Shift(fmt.Sprintf("%d", i), i)
		Expect(err).To(BeNil())
	}

	err = q.Shift(fmt.Sprintf("%d", 50), 50)
	Expect(err).ToNot(BeNil())

	for i := 0; i < 5; i++ {
		res = q.Unshift()
		Expect(res).ToNot(BeNil())
		Expect(res).To(Equal(4 - i))
	}
}

func TestFlush(t *testing.T) {
	var err error

	RegisterTestingT(t)

	q := NewQueue(5)
	Expect(q.size).To(Equal(0))
	Expect(len(q.index)).To(Equal(0))

	for i := 0; i < 5; i++ {
		err = q.Shift(fmt.Sprintf("%d", i), i)
		Expect(err).To(BeNil())
	}

	Expect(q.Size()).To(Equal(5))

	q.Flush()

	Expect(q.size).To(Equal(0))
	Expect(len(q.index)).To(Equal(0))
	Expect(q.Size()).To(Equal(0))
}

func TestQueueCreate(t *testing.T) {
	var err error
	var res interface{}

	RegisterTestingT(t)

	q := NewQueue(2)
	Expect(q.size).To(Equal(0))
	Expect(len(q.index)).To(Equal(0))

	res = q.Pop()
	Expect(res).To(BeNil())
	queueValuesQuickTest(q, 0, 0, 0, 0)

	err = q.Add("1", "asd")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 1, 1, 1, 0)

	err = q.Add("1", "asd")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 1, 1, 1, 0)

	err = q.Add("2", "qwe")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 2, 2, 0, 0)

	err = q.Add("2", "qwe")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 2, 2, 0, 0)

	err = q.Add("3", "123")
	Expect(err).ToNot(BeNil())
	queueValuesQuickTest(q, 2, 2, 0, 0)

	res = q.Pop()
	Expect(res).ToNot(BeNil())
	queueValuesQuickTest(q, 1, 1, 0, 1)

	err = q.Add("3", "123")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 2, 2, 1, 1)

	err = q.Add("2", "qwe")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 2, 2, 1, 1)

	err = q.Add("1", "asd")
	Expect(err).ToNot(BeNil())
	queueValuesQuickTest(q, 2, 2, 1, 1)

	res = q.Pop()
	Expect(res).ToNot(BeNil())
	queueValuesQuickTest(q, 1, 1, 1, 0)

	res = q.Pop()
	Expect(res).ToNot(BeNil())
	queueValuesQuickTest(q, 0, 0, 1, 1)

	res = q.Pop()
	Expect(res).To(BeNil())
	queueValuesQuickTest(q, 0, 0, 1, 1)
}

func TestQueueElementControl(t *testing.T) {
	var err error
	var found bool
	var res interface{}

	RegisterTestingT(t)

	q := NewQueue(3)
	Expect(q.size).To(Equal(0))
	Expect(len(q.index)).To(Equal(0))

	res = q.Pop()
	Expect(res).To(BeNil())
	queueValuesQuickTest(q, 0, 0, 0, 0)

	err = q.Shift("1", "asd")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 1, 1, 1, 0)

	err = q.Shift("1", "pwd")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 1, 1, 1, 0)

	err = q.Shift("2", "qwe")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 2, 2, 2, 0)

	err = q.ShiftOrReplace("2", "rtl")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 2, 2, 2, 0)

	err = q.PushOrReplace("1", "pwd")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 2, 2, 2, 0)

	found = q.IsQueued("2")
	Expect(found).To(BeTrue())

	found = q.IsQueued("3")
	Expect(found).To(BeFalse())

	res = q.Pop()
	Expect(res).ToNot(BeNil())
	Expect(res).To(Equal("pwd"))

	res = q.Pop()
	Expect(res).ToNot(BeNil())
	Expect(res).To(Equal("rtl"))
}

func TestAllOperations(t *testing.T) {
	var err error
	var res interface{}

	RegisterTestingT(t)

	q := NewQueue(2)

	res = q.Pop()
	Expect(res).To(BeNil())
	queueValuesQuickTest(q, 0, 0, 0, 0)

	err = q.Push("1", "asd")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 1, 1, 0, 1)

	err = q.Push("1", "asd")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 1, 1, 0, 1)

	err = q.Push("2", "qwe")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 2, 2, 0, 0)

	err = q.Push("2", "qwe")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 2, 2, 0, 0)

	err = q.Push("3", "123")
	Expect(err).ToNot(BeNil())
	queueValuesQuickTest(q, 2, 2, 0, 0)

	res = q.Pop()
	Expect(res).ToNot(BeNil())
	Expect(res).To(Equal("qwe"))
	queueValuesQuickTest(q, 1, 1, 0, 1)

	err = q.Push("3", "123")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 2, 2, 0, 0)

	err = q.Push("2", "qwe")
	Expect(err).ToNot(BeNil())
	queueValuesQuickTest(q, 2, 2, 0, 0)

	err = q.Push("1", "asd")
	Expect(err).To(BeNil())
	queueValuesQuickTest(q, 2, 2, 0, 0)

	res = q.Pop()
	Expect(res).ToNot(BeNil())
	Expect(res).To(Equal("123"))
	queueValuesQuickTest(q, 1, 1, 0, 1)

	res = q.Pop()
	Expect(res).ToNot(BeNil())
	Expect(res).To(Equal("asd"))
	queueValuesQuickTest(q, 0, 0, 0, 0)

	res = q.Pop()
	Expect(res).To(BeNil())
	queueValuesQuickTest(q, 0, 0, 0, 0)
}

func queueValuesQuickTest(q *Queue, size, indexLen, nextAdd, nextPop int) {
	Expect(q.size).To(Equal(size))
	Expect(q.Size()).To(Equal(size))
	Expect(len(q.index)).To(Equal(indexLen))
	Expect(q.nextAdd).To(Equal(nextAdd))
	Expect(q.nextPop).To(Equal(nextPop))
}
