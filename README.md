# Info

`go-structs` is a collection of common data structures

# Types

## Queue

  * fixed size
  * circular
  * burst on full or discard last element
  * element identifier for uniqueness
  * concurrent safe

## TODO

* N-Tree
* Linked List
