package structs

import "time"

var globalStats = NewStatsCollector()

// StatsCounterReset resets the counter value back to 0.
func StatsCounterReset(namespace string) {
	globalStats.CounterReset(namespace)
}

// StatsCounterIncr increment a counter stat.
func StatsCounterIncr(namespace string, value int64) {
	globalStats.CounterIncr(namespace, value)
}

// StatsCounterGet returns the current value of the provided stat namespace.
func StatsCounterGet(namespace string) int64 {
	return globalStats.CounterGet(namespace)
}

// StatsAverageAdd recalcs the average using the incremental average algorythm.
func StatsAverageAdd(namespace string, value float64) {
	globalStats.AverageAdd(namespace, value)
}

// StatsTimeAverageAdd will take the provided time and calculate the time diference to the present time and add that to the named average. Time unit is miliseconds.
func StatsTimeAverageAdd(namespace string, start time.Time) {
	globalStats.TimeAverageAdd(namespace, start)
}

// StatsGet returns a copy of the current application statistics.
func StatsGet() Stats {
	return globalStats.Get()
}

// StatsGetAndReset returns a copy of the current application statistics and resets them.
func StatsGetAndReset() Stats {
	return globalStats.GetAndReset()
}
